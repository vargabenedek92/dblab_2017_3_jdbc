package dal.impl;

import java.sql.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import dal.ActionResult;
import dal.DataAccessLayer;
import dal.exceptions.CouldNotConnectException;
import dal.exceptions.EntityNotFoundException;
import dal.exceptions.NotConnectedException;
import model.VideoHeader;
import model.Person;
import model.Video;
import model.Member;
import oracle.jdbc.proxy.annotation.Pre;

/**
 * Initial implementation for the DataAccessLayer for the 33-VIDEO exercise.
 */
public class VideoDal implements DataAccessLayer<VideoHeader, Video, Member> {
	private Connection connection;
	protected static final String driverName = "oracle.jdbc.driver.OracleDriver";
	protected static final String databaseUrl = "jdbc:oracle:thin:@rapid.eik.bme.hu:1521:szglab";

	private void checkConnected() throws NotConnectedException {
		if (connection == null) {
			throw new NotConnectedException();
		}
	}

	@Override
	public void connect(String username, String password) throws CouldNotConnectException, ClassNotFoundException {
		try {
			if (connection == null || !connection.isValid(30)) {
				if (connection == null) {
					// Load the specified database driver
					Class.forName(driverName);
				} else {
					connection.close();
				}

				// Create new connection and get metadata
				connection = DriverManager.getConnection(databaseUrl, username, password);
			}
		} catch (SQLException e) {
			throw new CouldNotConnectException();
		}
	}

	@Override
	public List<VideoHeader> search(String keyword) throws NotConnectedException {
		checkConnected();

		List<VideoHeader> queryRes = new ArrayList<>();
		String queryStr = "SELECT videoid, title, director FROM videos WHERE LOWER(title) LIKE LOWER(?) ESCAPE '\\'"; //kereses ne legyen case-sensitive

		keyword = keyword.replaceAll("%", "\\\\%");
		keyword = keyword.replaceAll("_", "\\\\_");
        keyword = "%" + keyword + "%"; //tetszoleges szamu karakter a keresoszo elejere es vegere is, ha a user csak a cim toredeket tudja

		try(PreparedStatement ps = connection.prepareStatement(queryStr)) {
			ps.setString(1, keyword);
			try (ResultSet rs = ps.executeQuery()){
				while(rs.next()){
					VideoHeader videoHeader = new VideoHeader();
					videoHeader.setVideoId(rs.getInt("videoid"));
					videoHeader.setTitle(rs.getString("title"));
					videoHeader.setDirector(rs.getString("director"));
					queryRes.add(videoHeader);
				}
				ps.close();
				return queryRes;
			}
		}
		catch (SQLException e){
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<Member> getStatistics() throws NotConnectedException {
		checkConnected();

		List<Member> queryRes = new ArrayList<>();
		String queryStr = "SELECT members.name, members.address " +
							"FROM videos, members, borrows " +
							"WHERE members.memberid = borrows.member " +
							"AND videos.videoid = borrows.video " +
							"AND borrows.dateofcreation < ADD_MONTHS(members.dateofbirth, 300) " +
							//mivel az ADD_YEARS fuggveny nem mukodott 25*12 honapot adtam hozza a datumhoz
							"AND videos.commentline LIKE '%Oscar%'";

		try(Statement st = connection.createStatement()) {
			try (ResultSet rs = st.executeQuery(queryStr)){
				while(rs.next()){
					Member member = new Member();
					member.setName(rs.getString("name"));
					member.setAddress(rs.getString("address"));
					queryRes.add(member);
				}
				st.close();
				return queryRes;
			}
		}
		catch (SQLException e){
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public boolean commit() throws NotConnectedException {
		checkConnected();
		return false;
	}

	@Override
	public boolean rollback() throws NotConnectedException {
		checkConnected();
		return false;
	}

	@Override
	public ActionResult insertOrUpdate(Video entity, Integer foreignKey)
			throws NotConnectedException, EntityNotFoundException {
		checkConnected();

		Boolean flag = false; //akkor false, ha nem volt egyezo id a tablaban a megadottal

        try {
            //ha van beirt videoId, akkor leellenorzom, hogy az szerepel-e mar az adatbazisban
            if (entity.getVideoId() != null){
                String queryStr = "SELECT COUNT(videoid) FROM videos WHERE videoid=?"; //megnezem a beirt ID darabszamat a tablaban
                PreparedStatement ps = connection.prepareStatement(queryStr);
                ps.setInt(1, entity.getVideoId());

                ResultSet rs = ps.executeQuery();
                if (rs.next()){
                    if(rs.getString(1).equals("0") == true){ //ha nem talalt egyezo ID-t
                        flag = false;
                    }
                    else{ //ha talalt egyezo ID-t
                        flag = true;
                    }
                }
                ps.close();
            }
            else { //ha nem is volt beirva semmi, akkor is false
                flag = false;
            }
        }
        catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }

        try {
            if (flag == true){ //ha volt egyezo ID, akkor egy meglevo elem modoitasa lesz
                String updateQueryStr = "UPDATE videos SET " +
                                            "title=?," +
                                            "appearance=?," +
                                            "director=?," +
                                            "duration=?," +
                                            "fee=?," +
                                            "commentline=?," +
                                            "type=? " +
                                        "WHERE videoid=?";

                PreparedStatement psUpdate = connection.prepareStatement(updateQueryStr);
                //kitoltjuk a megadott adatokkal a prepared statementet
                psUpdate.setString(1, entity.getTitle());
                psUpdate.setDate(2, java.sql.Date.valueOf(entity.getAppearance()));
                psUpdate.setString(3, entity.getDirector());
                psUpdate.setInt(4, entity.getDuration());
                psUpdate.setInt(5, entity.getFee());
                psUpdate.setString(6, entity.getCommentLine());
                psUpdate.setString(7, entity.getType());
                psUpdate.setInt(8, entity.getVideoId());
                //Vegrehajtjuk az utasitast
                psUpdate.executeUpdate();
                //jelezzuk, hogy frissites tortent
                return ActionResult.UpdateOccurred;
            }
            else{ //ha nem volt egyezo ID, akkor beszuras lesz, vagyis uj elem kerul az adatbazisba
                String queryStr = "INSERT INTO videos(videoid, title, appearance, director, duration, fee, commentline, type) " +
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement ps = connection.prepareStatement(queryStr);
                //kitoltjuk a megadott adatokkal a prepared statementet
                ps.setInt(1, entity.getVideoId());
                ps.setString(2, entity.getTitle());
                ps.setDate(3, java.sql.Date.valueOf(entity.getAppearance()));
                ps.setString(4, entity.getDirector());
                ps.setInt(5, entity.getDuration());
                ps.setInt(6, entity.getFee());
                ps.setString(7, entity.getCommentLine());
                ps.setString(8, entity.getType());
                //Vegrehajtjuk az utasitast
                ps.executeUpdate();
                //jelezzuk, hogy beszuras tortent
                return ActionResult.InsertOccurred;
            }
        }
        catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
	}

	@Override
	public boolean setAutoCommit(boolean value) throws NotConnectedException {
		checkConnected();
		return false;
	}

	@Override
	public void disconnect() {
		
	}

	@Override
	public List<Person> sampleQuery() throws NotConnectedException {
		checkConnected();
		List<Person> result = new ArrayList<>();
		try (Statement stmt = connection.createStatement()) {
			try (ResultSet rset = stmt.executeQuery("SELECT nev, szemelyi_szam FROM OKTATAS.SZEMELYEK "
					+ "ORDER BY NEV "
					+ "OFFSET 0 ROWS FETCH NEXT 20 ROWS ONLY")){
				while (rset.next()) {
					Person p = new Person(rset.getString("nev"), rset.getString("szemelyi_szam"));
					result.add(p);
				}
				return result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
