package application;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import dal.DataAccessLayer;
import dal.exceptions.CouldNotConnectException;
import dal.exceptions.EntityNotFoundException;
import dal.exceptions.NotConnectedException;
import dal.exceptions.ValidationException;
import dal.impl.VideoDal;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Paint;
import model.VideoHeader;
import model.Person;
import model.Video;
import model.Member;

/**
 * Class for implementing the logic.
 */
public class Controller implements Initializable {
	private DataAccessLayer<VideoHeader, Video, Member> dal;

	@FXML
	TextField searchTextField;
	@FXML
	TextField usernameField;
	@FXML
	TextField passwordField;
	@FXML
	Button connectButton;
	@FXML
	Label connectionStateLabel;
	@FXML
	ComboBox<ComboBoxItem<String>> sampleCombo;

	//Feladat 1
	@FXML
	TableView<VideoHeader> searchTable;
	@FXML
	TableColumn<VideoHeader, String> videoIdCol;
	@FXML
	TableColumn<VideoHeader, String> titleCol;
	@FXML
	TableColumn<VideoHeader, String> directorCol;

	//Feladat 2
    @FXML
    TextField idTextField;
    @FXML
    TextField titleTextField;
    @FXML
    TextField appearanceTextField;
    @FXML
    TextField directorTextField;
    @FXML
    TextField durationTextField;
    @FXML
    TextField feeTextField;
    @FXML
    TextField commentTextField;
    //@FXML
    //TextField typeTextField;

    //Feladat 3
    @FXML
    ComboBox<ComboBoxItem<String>> typeComboBox;
    @FXML
    Label parseStateLabel;

    //Feladat 5
    @FXML
    TableView<Member> statisticsTable;
    @FXML
    TableColumn<Member, String> memberNameCol;
    @FXML
    TableColumn<Member, String> memberAddressCol;



	public Controller() {
		dal = new VideoDal();
	}

	@FXML
	public void connectEventHandler(ActionEvent event) {
		//Getting the input from the UI.
		String username = usernameField.getText();
		String password = passwordField.getText();

		try {
			//Connect to the database, and update the UI
			dal.connect(username, password);
			connectionStateLabel.setText("Connection created!");
			connectionStateLabel.setTextFill(Paint.valueOf("green"));
		} catch (ClassNotFoundException e) {
			//Driver is not found
			connectionStateLabel.setText("JDBC driver not found!");
			connectionStateLabel.setTextFill(Paint.valueOf("red"));
		} catch (CouldNotConnectException e) {
			//Could not connect, e.g. invalid username or password
			connectionStateLabel.setText("Could not connect to the server!");
			connectionStateLabel.setTextFill(Paint.valueOf("red"));
		}
	}

	@FXML
	public void searchEventHandler() {
		try {
			List<VideoHeader> videoList = dal.search(searchTextField.getText());
			searchTable.setItems(FXCollections.observableArrayList(videoList));
		} catch (NotConnectedException e) {
            connectionStateLabel.setText(e.getMessage().toString());
            connectionStateLabel.setTextFill(Paint.valueOf("red"));
		}
		catch (RuntimeException e){
            connectionStateLabel.setText(e.getMessage().toString());
            connectionStateLabel.setTextFill(Paint.valueOf("red"));
        }
	}

	@FXML
	public void commitEventHandler() {
		//TODO: handle the click of the commit button.
	}

	@FXML
	public void editEventHandler() {
		try{
		    Video newVid = new Video();

		    //leellenorizzuk, hogy a megadott adatok a kovetelmenyeknek megfeleloek-e
		    newVid.parseVideoId(idTextField.getText());
            newVid.parseTitle(titleTextField.getText());
            newVid.parseAppearance(appearanceTextField.getText());
            newVid.parseDirector(directorTextField.getText());
            newVid.parseDuration(durationTextField.getText());
            newVid.parseFee(feeTextField.getText());
            newVid.parseCommentLine(commentTextField.getText());
            newVid.parseType(typeComboBox.getValue().getValue());

            connectionStateLabel.setText(String.valueOf(dal.insertOrUpdate(newVid, 0)));
            connectionStateLabel.setTextFill(Paint.valueOf("green"));

        }
        catch (EntityNotFoundException | ValidationException | RuntimeException e){
            parseStateLabel.setText(e.getMessage().toString());
		    parseStateLabel.setTextFill(Paint.valueOf("red"));
        }
        catch (NotConnectedException e){
            connectionStateLabel.setText(e.getMessage().toString());
            connectionStateLabel.setTextFill(Paint.valueOf("red"));
        }
	}

	@FXML
	public void statisticsEventHandler() {
		try {
		    List<Member> memberList = dal.getStatistics();
		    statisticsTable.setItems(FXCollections.observableArrayList(memberList));
			connectionStateLabel.setText("Statistics successfully downloaded.");
			connectionStateLabel.setTextFill(Paint.valueOf("green"));
        }
        catch (NotConnectedException e){
            connectionStateLabel.setText("First connect to server.");
            connectionStateLabel.setTextFill(Paint.valueOf("red"));
        }
        catch (RuntimeException e){
            connectionStateLabel.setText(e.getMessage().toString());
            connectionStateLabel.setTextFill(Paint.valueOf("red"));
            e.printStackTrace();
        }
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
        //Feladat 1
		videoIdCol.setCellValueFactory(new PropertyValueFactory<>("videoId"));
		titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
		directorCol.setCellValueFactory(new PropertyValueFactory<>("director"));

		//Feladat 3
        appearanceTextField.setPromptText("yyyy-MM-dd");
        typeComboBox.getItems().add(new ComboBoxItem<String>("Choose one...", ""));
        typeComboBox.getItems().add(new ComboBoxItem<String>("DVD", "D"));
        typeComboBox.getItems().add(new ComboBoxItem<String>("BluRay", "B"));
        typeComboBox.getSelectionModel().selectFirst();

        //Feladat 5
        memberNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        memberAddressCol.setCellValueFactory(new PropertyValueFactory<>("address"));

	}

	public void disconnect() {
		dal.disconnect();
	}

}