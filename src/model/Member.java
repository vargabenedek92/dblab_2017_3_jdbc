package model;

import java.time.LocalDate;

import dal.exceptions.ValidationException;

public class Member {
    public Member() {
    
    }
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}