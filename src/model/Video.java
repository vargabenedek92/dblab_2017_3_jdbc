package model;

import java.time.LocalDate;

import dal.exceptions.ValidationException;

public class Video {
    public Video() {
    
    }
    private Integer videoId;

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public void parseVideoId(String videoId) throws ValidationException {
        if(videoId.equals("") == true)
        {
            throw new ValidationException("Id must be completed.");
        }
        else if(videoId.matches("\\d{1,10}") == false)
        {
            throw new ValidationException("Id must contain only numbers and max. 10 digits.");
        }
        else
        {
            setVideoId(Integer.valueOf(videoId));
        }
    }
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void parseTitle(String title) throws ValidationException {
        if(title.equals("") == true)
        {
            throw new ValidationException("Title must be completed.");
        }
        else if((title.length() > 50) == true)
        {
            throw new ValidationException("Title must be max. 50 characters.");
        }
        else
        {
            setTitle(title);
        }
    }
    private String director;

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void parseDirector(String director) throws ValidationException {
        if(director.equals("") == true)
        {
            throw new ValidationException("Director field must be completed.");
        }
        else if((director.length() > 30) == true)
        {
            throw new ValidationException("Director must be max. 30 characters.");
        }
        else
        {
            setDirector(director);
        }
    }
    private LocalDate appearance;

    public LocalDate getAppearance() {
        return appearance;
    }

    public void setAppearance(LocalDate appearance) {
        this.appearance = appearance;
    }

    public void parseAppearance(String appearance) throws ValidationException {
        if(appearance.equals("") == true)
        {
            throw new ValidationException("Appearance field must be completed.");
        }
        else if(appearance.matches("\\d{4}-\\d{2}-\\d{2}") == false)
        {
            throw new ValidationException("Appearance must be in yyyy-MM-dd format.");
        }
        else
        {
            setAppearance(LocalDate.parse(appearance));
        }
    }
    private Integer fee;

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public void parseFee(String fee) throws ValidationException {
        if(fee.equals("") == true)
        {
            throw new ValidationException("Fee field must be completed.");
        }
        else if(fee.matches("\\d{1,6}") == false)
        {
            throw new ValidationException("Fee must contain only numbers and max. 6 digits.");
        }
        else{
            setFee(Integer.valueOf(fee));
        }
    }
    private Integer duration;

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public void parseDuration(String duration) throws ValidationException {
        if(duration.matches("\\d{1,3}") == false)
        {
            throw new ValidationException("Duration must contain only numbers and max. 3 digits.");
        }
        else{
            setDuration(Integer.valueOf(duration));
        }
    }
    private String commentLine;

    public String getCommentLine() {
        return commentLine;
    }

    public void setCommentLine(String commentLine) {
        this.commentLine = commentLine;
    }

    public void parseCommentLine(String commentLine) throws ValidationException {
        if(commentLine.equals("") == true)
        {
            setCommentLine(null);
        }
        else if((commentLine.length() > 150) == true)
        {
            throw new ValidationException("Commentline must contain max. 150 characters.");
        }
        else
        {
            setCommentLine(commentLine);
        }
    }
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void parseType(String type) throws ValidationException {
        if(type.equals("") == true)
        {
            throw new ValidationException("Type must be chosen from dropdown menu.");
        }
        else
        {
            setType(type);
        }
    }
}